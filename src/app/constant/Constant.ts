import { environment } from '../../environments/environment.pttinh';

export class Constant {
  public static URL_API = environment.backendIP;
  public static PORT = environment.backendPort;
  public static PREFIX = "api";

  public static DOWNLOAD_FILE_URL = [Constant.URL_API + ":" + Constant.PORT, Constant.PREFIX, 'download'].join("/");
  public static VIEW_AVATAR_URL = [Constant.URL_API + ":" + Constant.PORT, Constant.PREFIX, 'view'].join("/");
  public static USER_LOGS_URL = [Constant.URL_API + ":" + Constant.PORT, Constant.PREFIX, 'logs'].join("/")

  public static INTERVIEWEE_NOT_FOUND = "Interviewee not found";

  public static PAGE_NOT_FOUND = 404;
  public static SERVER_ERROR = 500;
  public static FORBIDDEN = 403;
  public static UNAUTHORIZED = 401;
  public static USERNAME_EXISTS = 400;
  public static DEFAULT_PAGE = 0;
  public static DEFAULT_SIZE = 10;
}
