import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { IntervieweeService } from '../service/interviewee.service';
import { RouterLinkActive, ActivatedRoute } from '@angular/router';
import { Interviewee } from '../models/interviewee.model';
import { ImageUploadComponent } from '../image-upload/image-upload.component';
import { TagsInputComponent } from '../util/tags-input/tags-input.component';
import { ToastrService } from 'ngx-toastr'
import { Constant } from '../constant/Constant'
@Component({
  selector: 'app-update-interviewee',
  templateUrl: './update-interviewee.component.html',
  styleUrls: ['./update-interviewee.component.scss']
})




export class UpdateIntervieweeComponent implements OnInit {
  @ViewChild(ImageUploadComponent) imageUpload: ImageUploadComponent;
  @ViewChild(TagsInputComponent) tagComponent: TagsInputComponent;

  techniquesSelected;
  positionsList: any;
  technique: string;
  role: boolean;
  interviewees: Interviewee;
  userID: any;
  date_schedule: any;
  time_schedule: any;
  files: any;
  rawFiles: any;
  statusColor;
  filesName = 'Select your files!';
  avatar: any = 'http://192.168.78.93:8080/api/avatar/default';

  constructor(private formbuilder: FormBuilder, private toastService: ToastrService,
    private intervieweeService: IntervieweeService,
    private router: ActivatedRoute) { }
  formUpdate: FormGroup;

  ngOnInit() {

    this.intervieweeService.getPositionList().subscribe(
      data => {
        this.positionsList = data.position_list;
      }
    )
    this.role = this.intervieweeService.getRoles();



    this.userID = this.router.snapshot.params.id;

    // get interviewee data
    this.intervieweeService.getIntervieweeByID(this.userID).subscribe((res) => {
      this.interviewees = res;
      this.technique = res.technique;
     this.statusColor = this.interviewees.status;

      const avatarBaseUrl = Constant.URL_API + ":" + Constant.PORT + `/api/${this.userID}/`;

      this.avatar = res.avatar ? avatarBaseUrl + res.avatar : this.avatar;
      this.filesName = res.cv ? res.cv : this.filesName;

      const gender = !res.gender ? 'male' : 'female';
      this.formUpdate = this.formbuilder.group({
        status: [res.status, Validators.required],
        fullname: [res.fullname, Validators.required],
        birthday: [res.birthday],
        gender: [gender],
        seasonName: [res.season_name],
        address: [res.address],
        phone_number: [res.phone_number],
        email: [res.email, Validators.compose([
          Validators.required,
          Validators.email])],
        timeInterview: [res.time_interview],
        englishSkill: [res.englishSkill],
        placeInterview: [res.place_interview, Validators.required],
        positionId: [res.position.id],
        note: [res.note],
      });

      // for testing , if change data in form 
      this.formUpdate.valueChanges.subscribe((data) => {
        // console.log(JSON.stringify(data));
      });
    });
  }



  onSubmitForm() {
    var sendObject = this.formUpdate.value;
    // console.log('avatar is' + this.imageUpload.croppedImage);
    // console.log('tag input is' + this.tagComponent.tags);
    // console.log('file is' + `${JSON.stringify(this.files[0].base64)}`);
    sendObject.cv = this.files ? this.files[0].base64 : '';
    sendObject.gender = sendObject.gender == 'female' ? true : false;
    sendObject = { ...sendObject, ...{ avatar: this.imageUpload.croppedImage }, ...{ technique: this.tagComponent.tags.join(',') }, ...{ userId: [2] } };

    console.log(sendObject);

    this.intervieweeService.updateInterviewee(this.userID, sendObject).subscribe(
      (res) => {
        this.toastService.success("<h5>Update Successfully!!!<h5>", "Notification");
      },
      (err) => {
       
      }
    );
  }


  // file encode go here
  onFileChanges(file) {
    console.log(file);
    this.filesName = file[0].name;
  }


  account_validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    'status': [
      { type: 'required', message: 'Status is required' }
    ],
    'fullname': [
      { type: 'required', message: 'Fullname is required' }
    ],
    'dob': [
      { type: 'required', message: 'Dob is required' }
    ],
    'room': [
      { type: 'required', message: 'Room is required' }
    ]
  }

  formatDate(date) {
    return date.split("/").join("-");
  }
  changeStatus(value) {
    this.statusColor = value;
  }

}
